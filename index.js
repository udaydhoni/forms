let form = document.getElementById('form');
let nameOfPerson = document.getElementById('name');
let emailOfPerson = document.getElementById('email');
let selectedMovie = document.getElementById('interests');
let colorChosen = document.getElementById('color');
let ratingGiven = document.getElementById('rating');
let radioBtnContainer =document.querySelector('.askgenre');
let genreSelected = '';
let terms = document.getElementById('terms');
let statusOfCheckBox = false;
let submit = document.getElementById('submit');
let modalBox = document.querySelector('.modal');
let closeButton = document.createElement('button');
    closeButton.innerText ='close';
let errorIngenre = document.querySelector('.genreerr');
let errorInTerms = document.querySelector('.termserr');

let dataObject = {}

function giveRadioValue (container = radioBtnContainer) {
    Array.from(container.children).forEach((elem)=>{
        if (elem.type == 'radio' && elem.checked) {
            genreSelected = elem.value;
        }
    })
}

function changeStatus (checkbox = terms) {
    if (checkbox.checked) {
        statusOfCheckBox = true
    } else {
        statusOfCheckBox = false
    }
}



form.addEventListener('submit', (event)=>{
    giveRadioValue();
    changeStatus();
    event.preventDefault();
     dataObject = {
        name: nameOfPerson.value,
        email: emailOfPerson.value,
        movie: selectedMovie.value,
        color: colorChosen.value,
        rating: ratingGiven.value,
        genre: genreSelected,
        status: statusOfCheckBox
    };
    if (dataObject['status']) {
        errorInTerms.innerText = '';
    } else {
        errorInTerms.innerText = '*Please accept our terms and conditions';
        errorInTerms.classList.add('warning');
    }
    for (let keys in dataObject) {
        if (dataObject[keys]) {
            if (keys == 'name') {
                let heading = document.createElement('h3');
                heading.textContent = `Hello ${dataObject['name']}`;
                modalBox.append(heading);
            } else if (keys == 'email'){
                let p = document.createElement('p');
                p.textContent = `Your Email: ${dataObject[keys]}`
                modalBox.append(p);
            } else if (keys == 'movie') {
                let p = document.createElement('p');
                p.textContent = `You love: ${dataObject[keys]}`
                modalBox.append(p);
            } else if (keys=='color'){
                let p = document.createElement('p');
                p.textContent = `Color: ${dataObject[keys]}`
                modalBox.append(p);
            } else if (keys == 'rating') {
                let p = document.createElement('p');
                p.textContent = `Rating: ${dataObject[keys]}`
                modalBox.append(p);
            } else if (keys == 'genre') {
                let p = document.createElement('p');
                p.textContent = `Book Genre: ${dataObject[keys]}  ${dataObject['color']}`
                modalBox.append(p);
            } else if (keys == 'status') {
                let p = document.createElement('p');
                p.textContent = '👉You agree to Terms and Condition'
                p.style.textDecoration = 'underline';
                modalBox.append(p);
            }
            
        } 
        else {
            modalBox.innerHTML = '';
            dataObject = {};
            return false;
        }
        
    }
    
    form.style.display = 'none';
    modalBox.style.display = 'block';
    modalBox.prepend(closeButton);
    closeButton.style.marginLeft = '500px';
    closeButton.style.border = 'none';
    closeButton.style.backgroundColor = 'white';
    closeButton.style.fontSize = '20px';
    closeButton.style.cursor = 'pointer';
    console.log(modalBox);
    console.log(dataObject);
})



closeButton.addEventListener('click',()=>{
    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'POST',
        body: JSON.stringify(dataObject),
        headers:{'Content-Type':'application/json'}
     }).then((x)=>{return x.json()}).then ((y)=>{console.log(y)});

    form.reset();
    modalBox.innerHTML = '';
    form.style.display = 'block';
    modalBox.style.display = 'none';
    // location.reload();
    console.log(modalBox);
    // })
    
})


nameOfPerson.addEventListener('blur',(event)=>{
    let errorInName = document.querySelector('.nameerr');
    errorInName.classList.add('warning');
    errorInName.innerText =''
    if (nameOfPerson.value == '') {
        errorInName.innerText = '* Name cannot be empty';
    }
})

emailOfPerson.addEventListener('blur',(event)=>{
    let errorInEmail = document.querySelector('.emailerr');
    errorInEmail.classList.add('warning');
    errorInEmail.innerText='';
    if (emailOfPerson.value == '') {
        errorInEmail.innerText = '* Email cannot be empty'
    } else if (!(emailOfPerson.value.includes('@'))){
        errorInEmail.innerText = '* Enter valid Email address'
    }
})











